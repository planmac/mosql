@rem ----------------------------------------------------
@rem batch file to build mosql using tcc on win64
@rem ----------------------------------------------------

@set MOSMLINCLUDE=E:\apps\mosml\include
@set SQLITEINCLUDE=E:\apps\sqlite3\src
@set SQLITELINK=-L.\winlib -lsqlite3 -lcamlrt	

@set CFLAGS=-O2 -fPIC -I%MOSMLINCLUDE% -I%SQLITEINCLUDE%
@set CCPATH=E:\dev\win32
@set CC=%CCPATH%\tcc.exe
@set LD=%CCPATH%\tcc.exe
@set DEF=%CCPATH%\tiny_impdef

:all
%CC% %CFLAGS% -c -o mosql.o mosql.c
%LD% -shared -o mosql.dll %SQLITELINK% mosql.o
@move mosql.dll .\winlib
%DEF% .\winlib\mosql.dll -o .\winlib\mosql.def
@del mosql.o
@goto end

:clean
del *.o
del *.dll

:test
mosml mosqltest.sml

:end
@echo Done!
