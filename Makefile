# Makefile for mosql dynamic library

MOSMLINCLUDE=/usr/local/include/mosml
SQLITEINCLUDE=/usr/include
SQLITELINK=-L/usr/lib64 -lsqlite3


CFLAGS=-Dunix -O2 -fPIC -I$(MOSMLINCLUDE) -I$(SQLITEINCLUDE)
CC=gcc
LD=ld

all: libmosql.so
	mv libmosql.so ./lib
	rm -f *.o

mosql.o: mosql.c
	$(CC) $(CFLAGS) -c -o mosql.o  mosql.c

libmosql.so: mosql.o
	$(LD) -shared -o libmosql.so $(SQLITELINK) mosql.o

clean:
	rm -f *.o
	rm -f *.so
	rm -f ./lib/*.so

test: all
	mosml mosqltest.sml
