/* Moscow ML basic SQLight library
   by planmac at gmail dot com
*/

/* c includes */
#include <ctype.h>              /* For toupper */
#include <stdlib.h>             /* For malloc  */
#include <string.h>             /* for strcpy  */
#include <stdio.h>              /* for sprintf */
#include <assert.h>             /* ??          */

/* Mosml includes */
#include <mlvalues.h>		/* For Val_unit, Long_val, String_val, ... */
#include <alloc.h>		/* For copy_string, alloc_string, ...      */
#include <memory.h>		/* For Modify, Push_roots, Pop_roots       */
#include <str.h>		/* For string_length                       */
//#include <interp.h>		/* For callback                            */

/* SQLite includes */
#include <sqlite3.h>

typedef struct sqlite3 sqlite3;
//typedef struct sqlite3_stmt sqlite3_stmt;

#ifdef WIN32
#define EXTERNML __declspec(dllexport)
#else
#define EXTERNML
#endif

/* Globals for SQLite */
sqlite3      *db;
sqlite3_stmt *stmt;


/* Utilities - borrowed from smlsqlite */

#define Nil_list  (Atom(0))
#define Head(xs)  (Field(xs, 0))
#define Tail(xs)  (Field(xs, 1))

static inline value make_cons(value elem, value tail) {
  value result;
  Push_roots(tmp, 2);
    tmp[0] = elem;
    tmp[1] = tail;
    result       = alloc(2, 1);     /* Allocate a cons cell, tag == 1 */
    Head(result) = tmp[0];          /* result is just allocated, thus */
    Tail(result) = tmp[1];          /* we don't need to use modify    */
  Pop_roots();
  return result;
}

/* moSQL functions */

/* mosml type: string -> unit */
EXTERNML value open_db(value fname) {
  char *cfname;

  cfname = String_val(fname);

  sqlite3_open( cfname, &db );
  if ( db==NULL || sqlite3_errcode(db)!=SQLITE_OK ) {
    fprintf(stderr, "Error: unable to open database \"%s\" %s\n",
	    cfname, sqlite3_errmsg(db) );
    exit(1);
  }
  
  return Val_unit;
}

/* mosml type: string -> unit */
EXTERNML value prepare(value sql) {
  char *csql = String_val(sql);
  int   clen = string_length(sql);
  const char *tail;
  int errcode = 0;  
    
  errcode = sqlite3_prepare( db, csql, clen, &stmt, &tail );
  if( errcode!=SQLITE_OK ) {
    fprintf(stderr, "Error: unable to prepare sql: \"%s\" %s\n",
	    csql, sqlite3_errmsg(db) );
    exit(1);
  }

  return Val_unit;
}

/* mosml type: unit -> string list */
EXTERNML value step(value dummy) {
  int errcode = 0;
  int ncols = 0;
  int col = 0;
  char *coltxt;
  value result = Nil_list;
  
  errcode = sqlite3_step( stmt );
  if( errcode==SQLITE_ROW ) {
    ncols = sqlite3_column_count( stmt );
    
    for( col=ncols-1; col>=0; col-- ) {
      coltxt = (char *)sqlite3_column_text(stmt, col);
      result = make_cons( copy_string(coltxt), result );
    }
  }

  return result;
}

/* mosml type: unit -> string list list */
EXTERNML value getrowsforsql(value csql) {
  value result = Nil_list;
  /* Suspect it might be easier to do in mosml */
  
  return result;
}


/* mosml type: unit -> unit */
EXTERNML value finalize(value v) {
  sqlite3_finalize(stmt);  // Invoking on a NULL pointer is a harmless noop
  return Val_unit;
}

/* mosml type: unit -> unit */
EXTERNML value close_db(value v) {
  if(db != NULL) sqlite3_close(db);
  return Val_unit;
}

