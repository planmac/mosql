(* Tests for libmosql.so *)

(* Load structures *)
app load ["Dynlib", "FileSys", "Array"];
open Dynlib;

load "Process";
local
  fun f (SOME a) = true
    | f (NONE)   = false;

  val libpath = if( f(Process.getEnv "SystemRoot")
                    orelse f(Process.getEnv "SYSTEMROOT") )
                then
	          "winlib/mosql.dll"
	        else
	          "lib/libmosql.so";  

in
  val dlh = dlopen { lib = Path.concat(FileSys.getDir (), libpath),
    	  	   flag = RTLD_LAZY,
		   global = false };
end;

(* mosql basic functions mapped to SQLite *)

val dbopen  : string -> unit        = app1 ( dlsym dlh "open_db"  );
val dbprep  : string -> unit        = app1 ( dlsym dlh "prepare"  );
val dbstep  : unit   -> string list = app1 ( dlsym dlh "step"     );
val dbfinal : unit   -> unit        = app1 ( dlsym dlh "finalize" );
val dbclose : unit   -> unit        = app1 ( dlsym dlh "close_db"  );

(* Some tests on the sample database *)

val _ = dbopen "test1.db";

(* Create table and insert some values *)
val _ = dbprep "DROP TABLE people;" ;
val _ = dbstep (); val _ = dbfinal ();

val _ = dbprep "CREATE TABLE people(id int, name text, role text);" ;
val _ = dbstep (); val _ = dbfinal ();

val inserts = ["(1, 'John',   'Bass' )",
	       "(2, 'Paul',   'Vocal')",
	       "(3, 'George', 'Lead' )",
	       "(4, 'Ringo',  'Drums')"];
	       
val _ = app (
	fn ins =>
	let
	    val _ = dbprep ("INSERT INTO people VALUES" ^ ins ^ ";");
	    val _ = dbstep ();
	in
	    dbfinal ()
	end)
	    inserts;

(* Retrieve some values *)
val _ = dbprep "select * from people where(role='Drums');";

val row1 : string list = dbstep ();  (* This is the row you want     *)
val row2 : string list = dbstep ();  (* Empty list indicates no more *)

val _ = dbfinal ();

(* Get a list of lists containing all results of a query *)
fun getrowsforsql (sql : string) : string list list =
  let
      fun f ([] , cum) = rev cum   (* Reverse the list before returning *)
       |  f (lst, cum) = f (dbstep(), lst::cum);
      val _ = dbprep sql;
      val res = f (dbstep(), []);  (* Need to calc result and finalize  *)
      val _ = dbfinal ();          (* the stmt, otherwise left hanging  *)
  in
      res
  end;
      
val all = getrowsforsql "SELECT * FROM people;";

		
(* Close the database *)
val _ = dbclose ();

